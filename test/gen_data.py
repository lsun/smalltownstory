#encoding:utf-8
# Created by Liang Sun <i@liangsun.org> in 2012

import urllib2
import re, json, time,hashlib, random
import MultipartPostHandler
from gevent import pool
from gevent import monkey
from urllib2 import URLError, HTTPError

monkey.patch_all()

def get_address():
    u = 1000000.0
    longitude = int(random.gauss(116307615, u)) 
    latitude = int(random.gauss(39983488, u)) 
    print "longitude=%d,latitude=%d" % (longitude, latitude)

    url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=false&language=zh-CN' % (latitude / u, longitude / u)

    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(), urllib2.HTTPRedirectHandler())

    str_content = opener.open(url).read()

    if str_content:
        content = json.loads(str_content)
        if content['status'] == 'OK':
            address = content['results'][0]['formatted_address']
            if address[0:2] == u'中国':
                address = address[2:]

            address = address.encode('utf-8')
            return (longitude, latitude, address)
        else:
            print content['status'].encode('utf-8') + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            time.sleep(60)
            return get_address()

def perform_request(opener, url, data):
    ans = False
    try:
        ans = opener.open(url, data=data).read()
    except HTTPError, e:
        print 'Error code: ', e.code
    except URLError, e:
        print e.reason
    return ans 

def store_one(tup, longitude, latitude, address):
    show_name, text = tup
    print show_name + " -:- " + text + " from: " + address

    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(), urllib2.HTTPRedirectHandler(), MultipartPostHandler.MultipartPostHandler)
    username = "test_" + "%d" % int(random.uniform(100000000, 1000000000))
    email = username + "@liang.sun"
    password = hashlib.sha256(username).hexdigest()
    url = 'http://story.hada.me:8080/api'
    data = '{"jsonrpc":"2.0","method":"register", "params":[{"email":"' + email + '", "password":"' + password + '"}],"id":1}'
    print 'Registering...'
    ret = perform_request(opener, url, data) 
    if not ret:
        return

    data = '{"jsonrpc":"2.0","method":"login", "params":[{"email":"' + email + '", "password":"' + password + '"}],"id":1}'
    print 'Logging in...'
    ret = perform_request(opener, url, data)
    if not ret:
        return

    data = '{"jsonrpc":"2.0","method":"modify_personal_information", "params":[{"show_name":"' + show_name + '"}],"id":1}'
    print 'Modifying show name...'
    ret = perform_request(opener, url, data)
    if not ret:
        return

    data = '{"jsonrpc":"2.0","method":"publish", "params":[{"text":"' + text
    data = data + '","location":{"longitude":%d,"latitude":%d},"address":"' % (longitude, latitude)
    data = data + address
    data = data + '","type":"thought"}],"id":1}'

    print 'Publishing...'
    ret = perform_request(opener, url, data)
    if not ret:
        return

def store(tups, longitude, latitude, address):
    size = len(tups)
    request_pool = pool.Pool(size)
    cnt = 0
    while cnt < size:
        request_pool.spawn(store_one, tups[cnt], longitude, latitude, address)
        cnt += 1

    request_pool.join()

url = 'http://weibo.com'
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(), urllib2.HTTPRedirectHandler())
content = opener.open(url).read()
while True:
    ms = re.findall('<div class="twit_item_content"> <a href=[^>]*>([^<]*)</a>：([^<]*)<div class="twit_item_time">', content)
    if ms:
        for m in ms:
            user, text = m
            print user + " -:- " + text

        
        longitude, latitude, address = get_address()
        store(ms, longitude, latitude, address)

    time.sleep(60)
