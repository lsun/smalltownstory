import unittest
import transaction
import json
from sqlalchemy import create_engine

from pyramid import testing

from .models import DBSession
from .aes import encrypt, decrypt
from .models import *
from .views import *


class TestSayHelloView(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()

    def test_it(self):
        request = testing.DummyRequest()
        info = say_hello_view(request, 'Liang', 'Sun')
        self.assertEqual(info, 'Hello, Liang Sun')


class TestRegisterView(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        engine = create_engine('sqlite://')
        
        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
        with transaction.manager:
            model = User(username='one@one.com', display_name='one', password='123456')
            DBSession.add(model)

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()

    def test_it(self):
        request = testing.DummyRequest()
        params = dict(email='test_test@liang.sun', password='test_test')
        info = register_view(request, params)
        self.assertEqual(info['error_code'], 0)

        request = testing.DummyRequest()
        params = dict(email='test_test@liang.sun')
        info = register_view(request, params)
        self.assertEqual(info['error_code'], 10)

        request = testing.DummyRequest()
        params = dict(email='test_test@liang.sun')
        info = register_view(request, params)
        self.assertEqual(info['error_code'], 10)

        request = testing.DummyRequest()
        params = dict(email='test_test@liangsun', password='seiru')
        info = register_view(request, params)
        self.assertEqual(info['error_code'], 4)
        
        request = testing.DummyRequest()
        params = dict(email=('012345678901234567890123456789012345678901234567890123456789'
                             '012345678901234567890123456789012345678901234567890123456789'
                             '012345678901234567890123456789012345678901234567890123456789'
                             '012345678901234567890123456789012345678901234567890123456789'
                             '01234567890123456789@liang.sun'), password='seiru')
        info = register_view(request, params)
        self.assertEqual(info['error_code'], 4)

        request = testing.DummyRequest()
        params = dict(email='i@liang.sun', password='')
        info = register_view(request, params)
        self.assertEqual(info['error_code'], 5)

        request = testing.DummyRequest()
        params = dict(email='one@one.com', password='1234')
        info = register_view(request, params)
        self.assertEqual(info['error_code'], 11)

class TestModifyView(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        from pyramid.session import UnencryptedCookieSessionFactoryConfig
        my_session_factory = UnencryptedCookieSessionFactoryConfig('itsaseekreet')
        self.config.set_session_factory(my_session_factory)
        engine = create_engine('sqlite://')

        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
        with transaction.manager:
            model = User(username='one@one.com', display_name='one', password='123456')
            DBSession.add(model)

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()

    def test_it(self):

        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(show_name='ok')
        info = modify_view(request, params)
        self.assertEqual(info['error_code'], 0)
        user = DBSession.query(User).filter(User.id == uid).first()
        self.assertEqual(user.display_name, 'ok')

        request = testing.DummyRequest()
        params = dict(name='ok')
        info = modify_view(request, params)
        self.assertEqual(info['error_code'], 10)

        request = testing.DummyRequest()
        params = dict(show_name='ok')
        info = modify_view(request, params)
        self.assertEqual(info['error_code'], 1)

        request = testing.DummyRequest()
        request.session['csrf'] = 'oij'
        request.session['uid'] = encrypt('1', request.session['csrf'][:16])
        params = dict(show_name='ok')
        info = modify_view(request, params)
        self.assertEqual(info['error_code'], 1)

        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        request.session['uid'] = encrypt('89', request.session['csrf'][:16])
        params = dict(show_name='ok')
        info = modify_view(request, params)
        self.assertEqual(info['error_code'], 2)

class TestPasswordView(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        engine = create_engine('sqlite://')

        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
        with transaction.manager:
            model = User(username='i@liang.sun', display_name='i', password='123456')
            DBSession.add(model)

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()

    def test_it(self):
        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(old_password='123456', new_password='654321')
        info = password_view(request, params)
        self.assertEqual(info['error_code'], 0)
        
        user = DBSession.query(User).filter(User.id == uid).first()
        self.assertEqual(user.password, '654321')

        request = testing.DummyRequest()
        params = dict(old_password='123456')
        info = password_view(request, params)
        self.assertEqual(info['error_code'], 10)
        
        request = testing.DummyRequest()
        params = dict(old_password='123456', new_password='654321')
        uid = 1
        request.session['uid'] = '123'
        info = password_view(request, params)
        self.assertEqual(info['error_code'], 1)

        request = testing.DummyRequest()
        request.session['csrf'] = 'abc'
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(old_password='123456', new_password='654321')
        info = password_view(request, params)
        self.assertEqual(info['error_code'], 1)

        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 100
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(old_password='123456', new_password='654321')
        info = password_view(request, params)
        self.assertEqual(info['error_code'], 2)

        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(old_password='1', new_password='654321')
        info = password_view(request, params)
        self.assertEqual(info['error_code'], 5)

class TestLoginView(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        engine = create_engine('sqlite://')

        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
        with transaction.manager:
            model = User(username='i@liang.sun', display_name='i', password='123456')
            DBSession.add(model)

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()

    def test_it(self):
        request = testing.DummyRequest()
        params = dict(password='123456', email='i@liang.sun')
        info = login_view(request, params)
        self.assertEqual(info['error_code'], 0)
        self.assertEqual(info['show_name'], 'i')
        self.assertEqual(info['user'], '1')
        self.assertEqual(len(request.session['csrf']), 40)
        self.assertEqual(request.session['uid'], encrypt('1', request.session['csrf'][:16]))
        
        request = testing.DummyRequest()
        params = dict(email='i@liang.sun')
        info = login_view(request, params)
        self.assertEqual(info['error_code'], 10)
        
        request = testing.DummyRequest()
        params = dict(email='i', password='11')
        info = login_view(request, params)
        self.assertEqual(info['error_code'], 4)
        
        request = testing.DummyRequest()
        params = dict(email='i@liang.sun', password='11')
        info = login_view(request, params)
        self.assertEqual(info['error_code'], 5)

class TestLogoutView(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        engine = create_engine('sqlite://')

        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
        with transaction.manager:
            model = User(username='i@liang.sun', display_name='i', password='123456')
            DBSession.add(model)

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()

    def test_it(self):
        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        info = logout_view(request)
        self.assertEqual(info['error_code'], 0)

        request = testing.DummyRequest()
        uid = 1
        request.session['uid'] = '123'
        info = logout_view(request)
        self.assertEqual(info['error_code'], 1)

class TestPublishView(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        engine = create_engine('sqlite://')

        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
        with transaction.manager:
            model = User(username='i@liang.sun', display_name='i', password='123456')
            DBSession.add(model)

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()

    def test_it(self):
        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(location=dict(longitude=1,latitude=2),address='the address', type='thought', text='the text')
        info = publish_view(request, params)
        self.assertEqual(info['error_code'], 0)
        self.assertEqual(info['story_id'], '1')

        story = DBSession.query(Story).filter(Story.id == 1).first()
        self.assertEqual(story.uid, 1)
        self.assertEqual(story.longitude, 1)
        self.assertEqual(story.latitude, 2)
        self.assertEqual(story.address, 'the address')
        self.assertEqual(story.text, 'the text')
        self.assertEqual(story.outline, 'the text')
        self.assertEqual(story.public, 1)
        self.assertEqual(story.valid, 1)
        self.assertEqual(story.has_photo, 0)
        self.assertEqual(story.has_text, 1)
        self.assertEqual(story.has_audio, 0)
        self.assertEqual(story.has_video, 0)
        ##################################################

        request = testing.DummyRequest()
        params = dict(location=dict(longitude=1,latitude=2),address='the address', type='thought', text='the text')
        uid = 1
        request.session['uid'] = '123'
        info = publish_view(request, params)
        self.assertEqual(info['error_code'], 1)
        ##################################################

        request = testing.DummyRequest()
        request.session['csrf'] = 'abc'
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(location=dict(longitude=1,latitude=2),address='the address', type='thought', text='the text')
        info = publish_view(request, params)
        self.assertEqual(info['error_code'], 1)
        ##################################################

        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(location=dict(longitude=1,latitude=2),address='the address')
        info = publish_view(request, params)
        self.assertEqual(info['error_code'], 10)
        ##################################################

        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(address='the address')
        info = publish_view(request, params)
        self.assertEqual(info['error_code'], 10)
        ##################################################

        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(location=dict(longitude=1,latitude=2),type='anything')
        info = publish_view(request, params)
        self.assertEqual(info['error_code'], 10)
        ##################################################

        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(location=dict(longitude=1,latitude=2),address='the address', type='thought')
        info = publish_view(request, params)
        self.assertEqual(info['error_code'], 0)
        self.assertEqual(info['story_id'], '2')

        story = DBSession.query(Story).filter(Story.id == 2).first()
        self.assertEqual(story.uid, 1)
        self.assertEqual(story.longitude, 1)
        self.assertEqual(story.latitude, 2)
        self.assertEqual(story.address, 'the address')
        self.assertEqual(story.text, '')
        self.assertEqual(story.outline, '')
        self.assertEqual(story.public, 1)
        self.assertEqual(story.valid, 1)
        self.assertEqual(story.has_photo, 0)
        self.assertEqual(story.has_text, 0)
        self.assertEqual(story.has_audio, 0)
        self.assertEqual(story.has_video, 0)
        ##################################################


class TestEditView(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        engine = create_engine('sqlite://')

        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
        with transaction.manager:
            model = User(username='i@liang.sun', display_name='i', password='123456')
            DBSession.add(model)
            model2 = User(username='2@liang.sun', display_name='2', password='234234')
            DBSession.add(model2)
            story = Story(uid=1,longitude=1,latitude=2,address='address',outline='outline',text='text',
                          date=342,public=1,valid=1,has_photo=0,has_text=1,has_audio=0,has_video=0)
            DBSession.add(story)

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()

    def test_it(self):
        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(story_id=1, text='modified text')
        info = edit_view(request, params)
        self.assertEqual(info['error_code'], 0)

        story = DBSession.query(Story).filter(Story.id == 1).first()
        self.assertEqual(story.text, 'modified text')
        self.assertEqual(story.outline, 'modified text')
        self.assertEqual(story.has_text, 1)
        ####################################

        request = testing.DummyRequest()
        uid = 1
        request.session['uid'] = '123'
        params = dict(story_id=1, text='modified text')
        info = edit_view(request, params)
        self.assertEqual(info['error_code'], 1)
        ####################################

        request = testing.DummyRequest()
        request.session['csrf'] = 'abc'
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(story_id=1, text='modified text')
        info = edit_view(request, params)
        self.assertEqual(info['error_code'], 1)
        ####################################

        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(text='modified text')
        info = edit_view(request, params)
        self.assertEqual(info['error_code'], 10)
        ####################################

        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(story_id=100, text='modified text')
        info = edit_view(request, params)
        self.assertEqual(info['error_code'], 3)
        ####################################

        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 2
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(story_id=1, text='modified text')
        info = edit_view(request, params)
        self.assertEqual(info['error_code'], 6)
        ####################################

        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(story_id=1, text='')
        info = edit_view(request, params)
        self.assertEqual(info['error_code'], 0)

        story = DBSession.query(Story).filter(Story.id == 1).first()
        self.assertEqual(story.text, '')
        self.assertEqual(story.outline, '')
        self.assertEqual(story.has_text, 0)
        ####################################

class TestDeleteStoryView(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        engine = create_engine('sqlite://')

        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
        with transaction.manager:
            model = User(username='i@liang.sun', display_name='i', password='123456')
            DBSession.add(model)
            model2 = User(username='2@liang.sun', display_name='2', password='234234')
            DBSession.add(model2)
            story = Story(uid=1,longitude=1,latitude=2,address='address',outline='outline',text='text',
                          date=342,public=1,valid=1,has_photo=0,has_text=1,has_audio=0,has_video=0)
            DBSession.add(story)
            story2 = Story(uid=1,longitude=1,latitude=2,address='address',outline='outline',text='text',
                           date=342,public=1,valid=1,has_photo=0,has_text=1,has_audio=0,has_video=0)
            DBSession.add(story2)

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()

    def test_it(self):
        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(story_id=1)
        info = delete_story_view(request, params)
        self.assertEqual(info['error_code'], 0)

        story = DBSession.query(Story).filter(Story.id == 1).first()
        self.assertEqual(story, None)
        ####################################

        request = testing.DummyRequest()
        uid = 1
        request.session['uid'] = '123'
        params = dict(story_id=1)
        info = delete_story_view(request, params)
        self.assertEqual(info['error_code'], 1)
        ####################################

        request = testing.DummyRequest()
        request.session['csrf'] = 'abc'
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(story_id=1)
        info = delete_story_view(request, params)
        self.assertEqual(info['error_code'], 1)
        ####################################

        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(i=1)
        info = delete_story_view(request, params)
        self.assertEqual(info['error_code'], 10)
        ####################################
        
        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(story_id=100)
        info = delete_story_view(request, params)
        self.assertEqual(info['error_code'], 3)
        ####################################

        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 2
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(story_id=2)
        info = delete_story_view(request, params)
        self.assertEqual(info['error_code'], 6)
        ####################################


class TestDeleteResponseView(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        engine = create_engine('sqlite://')

        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
        with transaction.manager:
            model = User(username='i@liang.sun', display_name='i', password='123456')
            DBSession.add(model)
            model2 = User(username='2@liang.sun', display_name='2', password='234234')
            DBSession.add(model2)
            story = Story(uid=1,longitude=1,latitude=2,address='address',outline='outline',text='text',
                          date=342,public=1,valid=1,has_photo=0,has_text=1,has_audio=0,has_video=0)
            DBSession.add(story)
            story2 = Story(uid=1,longitude=1,latitude=2,address='address',outline='outline',text='text',
                           date=342,public=1,valid=1,has_photo=0,has_text=1,has_audio=0,has_video=0)
            DBSession.add(story2)
            
            comment = Comment(sid=1,uid=1,text='Comment....',to=1,date=22232)
            DBSession.add(comment)

            comment2 = Comment(sid=1,uid=2,text='Comment....',to=1,date=22232)
            DBSession.add(comment2)

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()

    def test_it(self):
        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(response_id=1)
        info = delete_response_view(request, params)
        self.assertEqual(info['error_code'], 0)

        comment = DBSession.query(Comment).filter_by(id=1).first()
        self.assertEqual(comment, None)
        ####################################

        request = testing.DummyRequest()
        uid = 1
        request.session['uid'] = '123'
        params = dict(response_id=1)
        info = delete_response_view(request, params)
        self.assertEqual(info['error_code'], 1)
        ####################################

        request = testing.DummyRequest()
        request.session['csrf'] = 'abc'
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(response_id=1)
        info = delete_response_view(request, params)
        self.assertEqual(info['error_code'], 1)
        ####################################

        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(i=1)
        info = delete_response_view(request, params)
        self.assertEqual(info['error_code'], 10)
        ####################################
        
        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(response_id=100)
        info = delete_response_view(request, params)
        self.assertEqual(info['error_code'], 12)
        ####################################

        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(response_id=2)
        info = delete_response_view(request, params)
        self.assertEqual(info['error_code'], 6)
        ####################################

class TestResponseView(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        engine = create_engine('sqlite://')

        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
        with transaction.manager:
            model = User(username='i@liang.sun', display_name='i', password='123456')
            DBSession.add(model)
            model2 = User(username='2@liang.sun', display_name='2', password='234234')
            DBSession.add(model2)
            story = Story(uid=1,longitude=1,latitude=2,address='address',outline='outline',text='text',
                          date=342,public=1,valid=1,has_photo=0,has_text=1,has_audio=0,has_video=0)
            DBSession.add(story)
            story2 = Story(uid=2,longitude=1,latitude=2,address='address',outline='outline',text='text',
                           date=342,public=1,valid=1,has_photo=0,has_text=1,has_audio=0,has_video=0)
            DBSession.add(story2)

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()

    def test_it(self):
        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1

        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        
        params = dict(story_id=1,text='this is a comment',to=2)
        info = response_view(request, params)
        self.assertEqual(info['error_code'], 0)

        params = dict(story_id=2,text='this is a comment')
        info = response_view(request, params)
        self.assertEqual(info['error_code'], 0)

        comment = DBSession.query(Comment).filter(Comment.sid == 1).first()
        self.assertEqual(comment.uid, 1)
        self.assertEqual(comment.text, 'this is a comment')
        self.assertEqual(comment.to, 2)

        notif1 = DBSession.query(Notification).filter_by(to=1).first()
        self.assertEqual(notif1, None)

        num = DBSession.query(Notification).filter_by(to=2).count()
        self.assertEqual(num, 2)

        ####################################

        request = testing.DummyRequest()
        uid = 1
        request.session['uid'] = '123'
        params = dict(story_id=1,text='this is a comment',to=2)
        info = response_view(request, params)
        self.assertEqual(info['error_code'], 1)
        ####################################

        request = testing.DummyRequest()
        request.session['csrf'] = 'abc'
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(story_id=1,text='this is a comment',to=2)
        info = response_view(request, params)
        self.assertEqual(info['error_code'], 1)
        ####################################

        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(text='this is a comment',to=2)
        info = response_view(request, params)
        self.assertEqual(info['error_code'], 10)
        ####################################

        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(story_id=2,text='this is a comment')
        info = response_view(request, params)
        self.assertEqual(info['error_code'], 0)

        comment = DBSession.query(Comment).filter(Comment.sid == 2).first()
        self.assertEqual(comment.uid, 1)
        self.assertEqual(comment.text, 'this is a comment')
        self.assertEqual(comment.to, 0)
        ####################################

class TestGetOutlinesByGPSView(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        engine = create_engine('sqlite://')

        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
        with transaction.manager:
            model = User(username='i@liang.sun', display_name='i', password='123456')
            DBSession.add(model)
            model2 = User(username='2@liang.sun', display_name='2', password='234234')
            DBSession.add(model2)
            story = Story(uid=1,longitude=1,latitude=2,address='address',outline='outline',text='text',
                          date=342,public=1,valid=1,has_photo=0,has_text=1,has_audio=0,has_video=0)
            DBSession.add(story)
            story2 = Story(uid=2,longitude=3,latitude=4,address='address2',outline='outline2',text='text2',
                           date=332,public=1,valid=1,has_photo=1,has_text=1,has_audio=0,has_video=0)
            DBSession.add(story2)
            story3 = Story(uid=2,longitude=5,latitude=6,address='address3',outline='outline3',text='text3',
                           date=332,public=0,valid=1,has_photo=0,has_text=1,has_audio=0,has_video=0)
            DBSession.add(story3)

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()

    def test_it(self):
        request = testing.DummyRequest()
        params = dict(longitude=0,latitude=0,offset=0)
        info = get_outlines_by_gps_view(request, params)
        self.assertEqual(info['error_code'], 0)
        self.assertEqual(len(info['stories_breviary']), 2)
        self.assertEqual(info['stories_breviary'][0]['story_id'], '1')
        self.assertEqual(info['stories_breviary'][1]['story_id'], '2')
        ####################################

        request = testing.DummyRequest()
        params = dict(offset=0)
        info = get_outlines_by_gps_view(request, params)
        self.assertEqual(info['error_code'], 10)
        ####################################

class TestGetOutlinesByTimeView(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        engine = create_engine('sqlite://')

        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
        with transaction.manager:
            model = User(username='i@liang.sun', display_name='i', password='123456')
            DBSession.add(model)
            model2 = User(username='2@liang.sun', display_name='2', password='234234')
            DBSession.add(model2)
            story = Story(uid=1,longitude=1,latitude=2,address='address',outline='outline',
                          text='text',date=342,public=1,valid=1,has_photo=0,has_text=1,has_audio=0,has_video=0)
            DBSession.add(story)
            story2 = Story(uid=1,longitude=3,latitude=4,address='address2',outline='outline2',
                           text='text2',date=332,public=1,valid=1,has_photo=1,has_text=1,has_audio=0,has_video=0)
            DBSession.add(story2)
            story3 = Story(uid=2,longitude=5,latitude=6,address='address3',outline='outline3',text='text3',
                           date=332,public=0,valid=1,has_photo=0,has_text=1,has_audio=0,has_video=0)
            DBSession.add(story3)

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()

    def test_it(self):
        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(type='newer_than',time=0)
        info = get_outlines_by_time_view(request, params)
        self.assertEqual(info['error_code'], 0)
        self.assertEqual(len(info['stories_breviary']), 2)
        self.assertEqual(info['stories_breviary'][0]['story_id'], '1')
        self.assertEqual(info['stories_breviary'][1]['story_id'], '2')
        ####################################

        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(time=0)
        info = get_outlines_by_time_view(request, params)
        self.assertEqual(info['error_code'], 10)
        ####################################

class TestGetStoryView(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        engine = create_engine('sqlite://')

        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
        with transaction.manager:
            model = User(username='i@liang.sun', display_name='i', password='123456')
            DBSession.add(model)
            model2 = User(username='2@liang.sun', display_name='2', password='234234')
            DBSession.add(model2)
            story = Story(uid=1,longitude=1,latitude=2,address='address',outline='outline',
                          text='text',date=342,public=1,valid=1,
                          has_photo=1,has_text=1,has_audio=0,has_video=0)
            DBSession.add(story)
            comment = Comment(sid=1,uid=2,text='Comment....',to=1,date=22232)
            DBSession.add(comment)

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()

    def test_it(self):
        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        params = dict(story_id='1')
        info = get_story_view(request, params)
        self.assertEqual(info['error_code'], 0)
        self.assertEqual(info['story_id'], '1')
        self.assertEqual(info['text'], 'text')
        self.assertEqual(info['user'], '1')
        ####################################

class TestUploadFileView(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        engine = create_engine('sqlite://')

        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
        with transaction.manager:
            model = User(username='i@liang.sun', display_name='i', password='123456')
            DBSession.add(model)
            model2 = User(username='2@liang.sun', display_name='2', password='234234')
            DBSession.add(model2)
            story = Story(uid=1,longitude=1,latitude=2,address='address',outline='outline',
                          text='text',date=342,public=1,valid=1,
                          has_photo=1,has_text=1,has_audio=0,has_video=0)
            DBSession.add(story)
            comment = Comment(sid=1,uid=2,text='Comment....',to=1,date=22232)
            DBSession.add(comment)

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()

    def test_it(self):
        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        info = upload_file_view(request).body
        info = json.loads(info)
        self.assertEqual(info['result']['error_code'], 8)
        ####################################

class TestUploadAvatarView(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        engine = create_engine('sqlite://')

        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
        with transaction.manager:
            model = User(username='i@liang.sun', display_name='i', password='123456')
            DBSession.add(model)
            model2 = User(username='2@liang.sun', display_name='2', password='234234')
            DBSession.add(model2)
            story = Story(uid=1,longitude=1,latitude=2,address='address',outline='outline',
                          text='text',date=342,public=1,valid=1,
                          has_photo=1,has_text=1,has_audio=0,has_video=0)
            DBSession.add(story)
            comment = Comment(sid=1,uid=2,text='Comment....',to=1,date=22232)
            DBSession.add(comment)

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()

    def test_it(self):
        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        info = upload_avatar_view(request).body
        info = json.loads(info)
        self.assertEqual(info['result']['error_code'], 8)
        
        ####################################

class TestGetNotificationsView(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        engine = create_engine('sqlite://')
        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
        with transaction.manager:
            notification = Notification(to=1,sid=123,origin='Good.',is_author=1)
            DBSession.add(notification)

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()

    def test_it(self):
        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        info = get_notifications_view(request)
        self.assertEqual(info['error_code'], 0)
        self.assertEqual(len(info['msgs']), 1)
        self.assertEqual(info['msgs'][0]['story_id'], 123)
        self.assertEqual(info['msgs'][0]['origin'], 'Good.')
        self.assertEqual(info['msgs'][0]['type'], 1)
        self.assertEqual(info['msgs'][0]['count'], 1)
        ####################################

class TestDeleteNotificationsView(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        engine = create_engine('sqlite://')

        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
        with transaction.manager:
            notification = Notification(to=1,sid=123,origin='Good.',is_author=1)
            DBSession.add(notification)
            notification = Notification(to=1,sid=234,origin='OK',is_author=0)
            DBSession.add(notification)


    def tearDown(self):
        DBSession.remove()
        testing.tearDown()

    def test_it(self):
        num = DBSession.query(Notification).count()
        self.assertEqual(num, 2)

        request = testing.DummyRequest()
        request.session['csrf'] = request.session.new_csrf_token()
        uid = 1
        request.session['uid'] = encrypt('%d' % uid, request.session['csrf'][:16])
        
        params = dict(story_ids=['123', '234'])

        info = delete_notifications_view(request, params)

        self.assertEqual(info['error_code'], 0)
        
        num = DBSession.query(Notification).count()
        self.assertEqual(num, 0)
        nn = DBSession.query(Notification).filter_by(to=1).first()
        self.assertEqual(nn, None)

        ####################################






