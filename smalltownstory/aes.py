#encoding:utf-8
#Created by Liang Sun in 2012

import os, hashlib, random

from Crypto.Cipher import AES

password = 'alazyrabbit'
key = hashlib.sha256(password).digest()

#iv means initial vector
#Do not use the same iv all the time!!!
def encrypt(in_string, iv):
    #iv should be 16 bytes
    if len(iv) != 16:
        return

    #in_string should not contain space
    if in_string.find(' ') >= 0:
        return

    encryptor = AES.new(key, AES.MODE_CBC, iv)
    in_string = in_string + ' ' * (16 - len(in_string) % 16)
    en_string = encryptor.encrypt(in_string)
    out_string = ''
    for c in en_string:
        out_string += '%x%x' % (((ord(c) & 0xF0) >> 4), (ord(c) & 0x0F))

    return out_string

def decrypt(in_string, iv):
    #iv should be 16 bytes
    if len(iv) != 16:
        return

    if len(in_string) % 32 != 0:
        return
    en_string = ''
    for i in range(len(in_string) / 2):
        en_string += chr((int(in_string[i * 2], 16) << 4) | int(in_string[i * 2 + 1], 16))

    decryptor = AES.new(key, AES.MODE_CBC, iv)
    out_string = decryptor.decrypt(en_string)
    out_string = out_string[:out_string.find(' ')]
    return out_string


#Test
if __name__ == '__main__':
    iv = ''.join(chr(random.randint(0, 0xFF)) for i in range(16))
    in_string = '0123456789012'
    print in_string
    en_string = encrypt(in_string, iv)
    print en_string
    print int(decrypt(en_string, iv))

    in_string = '1'
    print in_string
    en_string = encrypt(in_string, iv)
    print en_string
    print int(decrypt(en_string, iv))
