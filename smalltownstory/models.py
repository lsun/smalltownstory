from sqlalchemy import (
    Column,
    Integer,
    Boolean,
    )

from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
    )

from zope.sqlalchemy import ZopeTransactionExtension

from sqlalchemy.dialects.mysql import TEXT, VARCHAR

DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    username = Column(VARCHAR(300, charset='utf8'), unique=True)
    display_name = Column(VARCHAR(300, charset='utf8'))
    password = Column(VARCHAR(300, charset='utf8'))

    def __init__(self, username, display_name, password):
        self.username = username
        self.display_name = display_name
        self.password = password

class Story(Base):
    __tablename__ = 'stories'
    
    id = Column(Integer, primary_key=True)
    uid = Column(Integer, nullable=False)
    longitude = Column(Integer)
    latitude = Column(Integer)
    address = Column(VARCHAR(500, charset='utf8'))
    outline = Column(TEXT(charset='utf8'))
    text = Column(TEXT(charset='utf8'))
    date = Column(Integer)
    photo_width = Column(Integer)
    photo_height = Column(Integer)
    public = Column(Boolean)
    valid = Column(Boolean)
    has_photo = Column(Boolean)
    has_text = Column(Boolean)
    has_audio = Column(Boolean)
    has_video = Column(Boolean)

    def __init__(self, uid, longitude, latitude, address, outline, text, date, public, valid, has_photo, has_text, has_audio, has_video):
        self.uid = uid
        self.longitude = longitude
        self.latitude = latitude
        self.address = address
        self.outline = outline
        self.text = text
        self.date = date
        self.photo_width = 0
        self.photo_height = 0
        self.public = public
        self.valid = valid
        self.has_photo = has_photo
        self.has_text = has_text
        self.has_audio = has_audio
        self.has_video = has_video

class Comment(Base):
    __tablename__ = 'comments'

    id = Column(Integer, primary_key=True)
    sid = Column(Integer, nullable=False)
    uid = Column(Integer, nullable=False)
    text = Column(TEXT(charset='utf8'))
    date = Column(Integer)
    to = Column(Integer)

    def __init__(self, sid, uid, text, date, to):
        self.uid = uid
        self.sid = sid
        self.text = text
        self.date = date
        self.to = to

class Notification(Base):
    __tablename__ = 'notifications'

    id = Column(Integer, primary_key=True)
    to = Column(Integer, nullable=False)
    sid = Column(Integer, nullable=False)
    origin = Column(TEXT(charset='utf8'))
    count = Column(Integer)
    is_author = Column(Boolean)

    def __init__(self, to, sid, origin, is_author):
        self.to = to
        self.sid = sid
        self.origin = origin
        self.count = 1
        self.is_author = is_author


