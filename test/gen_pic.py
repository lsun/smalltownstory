#encoding:utf-8
# Created by Liang Sun <i@liangsun.org> in 2012

import urllib2
import os
from shutil import move
import re, json, time,hashlib, random
import MultipartPostHandler
from urllib2 import URLError, HTTPError

def get_address():
    u = 100000.0
    v = 1000000.0
    lng = 116307615
    lat = 39983488

    longitude = int(random.gauss(lng, u)) 
    latitude = int(random.gauss(lat, u)) 
    print "longitude=%d,latitude=%d" % (longitude, latitude)

    url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=false&language=zh-CN' % (latitude / v, longitude / v)
    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(), urllib2.HTTPRedirectHandler())
    
    try:
        str_content = opener.open(url).read()
    except HTTPError, e:
        print 'Error code: ', e.code
        time.sleep(36)
        return get_address()
    except URLError, e:
        print e.reason
        time.sleep(36)
        return get_address()
    
    if str_content:
        content = json.loads(str_content)
        if content['status'] == 'OK':
            address = content['results'][0]['formatted_address']
            if address[0:2] == u'中国':
                address = address[2:]
            if address.find(' ') > 0:
                address = address[:address.find(' ')]

            address = address.encode('utf-8')
            return (longitude, latitude, address)
        else:
            print content['status'].encode('utf-8') + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            time.sleep(36)
            return get_address()

def get_addresses():
    ans = []
    for i in range(3):
        ans.append(get_address())

    return ans

def perform_request(opener, url, data):
    ans = False
    try:
        ans = opener.open(url, data=data).read()
    except HTTPError, e:
        print 'Error code: ', e.code
    except URLError, e:
        print e.reason
    return ans 

def store_one(show_name, text, img_file, avatar_file, address_tup):
    longitude, latitude, address = address_tup
    print '[' + show_name + ']' + text + '[' + address + ']'

    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(), urllib2.HTTPRedirectHandler(), MultipartPostHandler.MultipartPostHandler)
    username = "test_" + "%d" % int(random.uniform(100000000, 1000000000))
    email = username + "@liang.sun"
    password = hashlib.sha256(username).hexdigest()
    url = 'http://story.hada.me:8080/api'
    data = '{"jsonrpc":"2.0","method":"register", "params":[{"email":"' + email + '", "password":"' + password + '"}],"id":1}'
    #print 'Registering...'
    ret = perform_request(opener, url, data) 
    if not ret:
        return

    data = '{"jsonrpc":"2.0","method":"login", "params":[{"email":"' + email + '", "password":"' + password + '"}],"id":1}'
    #print 'Logging in...'
    ret = perform_request(opener, url, data)
    if not ret:
        return

    data = '{"jsonrpc":"2.0","method":"modify_personal_information", "params":[{"show_name":"' + show_name + '"}],"id":1}'
    #print 'Modifying show name...'
    ret = perform_request(opener, url, data)
    if not ret:
        return

    data = '{"jsonrpc":"2.0","method":"publish", "params":[{"text":"' + text
    data = data + '","location":{"longitude":%d,"latitude":%d},"address":"' % (longitude, latitude)
    data = data + address
    data = data + '","type":"photo"}],"id":1}'

    #print 'Publishing...'
    ret = perform_request(opener, url, data)
    if not ret:
        return

    result = json.loads(ret)['result']
    if result['error_code'] != 0:
        return
    sid = result['story_id'].encode('utf-8') #For response

    print 'Uploading image...'
    url = 'http://story.hada.me:8080/file'
    filename = sid + '.jpg'
    move(img_file, filename)
    data = {'file':open(filename, 'rb')}
    ret = perform_request(opener, url, data)
    if not ret:
        return

    print 'Uploading avatar...'
    url = 'http://story.hada.me:8080/avatar'
    data = {'file':open(avatar_file, 'rb')}
    ret = perform_request(opener, url, data)
    if not ret:
        return

    #Clear tmp file
    os.remove(filename)
    os.remove(avatar_file)

url = 'http://huaban.com'

while True:
    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(), urllib2.HTTPRedirectHandler())
    try:
        content = opener.open(url).read()
    except HTTPError, e:
        print 'Error code: ', e.code
    except URLError, e:
        print e.reason

    addresses = get_addresses()
    msg = re.findall('<a href="[^"]+" class="img[^"]*"><img src="([^"]+)" width="(\d+)" height="(\d+)"/></a><p class="description">([^<]+)</p><p class="[^"]+"></p><div class="[^"]+"><p><a href="[^"]+" title="[^"]+" class="[^"]+"><img src="([^"]+)"/></a><a href="[^"]+" class="[^"]+">([^<]+)</a>', content)
    if msg: 
        i = 0
        for m in msg: 
            img, width, height, desc, avatar, user = m 
            if img[0:7] != 'http://':
                img = url + img

            if avatar[0:7] != 'http://':
                avatar = url + avatar

            print "(" + width + "x" + height + "):" + desc + "[" + user + "]" 

            suffix_len = len(img) - img.rfind('.') - 1
            if suffix_len == 3 or suffix_len == 4:
                suffix = img[img.rfind('.'):]
            else:
                suffix = '.jpg'

            img_file = hashlib.sha256(img).hexdigest() + suffix
            f = open(img_file, 'wb')
            try:
                f.write(opener.open(img).read())
            except HTTPError, e:
                print '!!!!!!!!!!Error code: ', e.code
                continue
            except URLError, e:
                print '!!!!!!!!!!' + e.reason
                continue
            f.close()

            suffix_len = len(avatar) - avatar.rfind('.') - 1
            if suffix_len == 3 or suffix_len == 4:
                suffix = avatar[avatar.rfind('.'):]
            else:
                suffix = '.jpg'
            avatar_file = hashlib.sha256(avatar).hexdigest() + suffix
            f = open(avatar_file, 'wb')
            try:
                f.write(opener.open(avatar).read())
            except HTTPError, e:
                print '!!!!!!!!!!!Error code: ', e.code
                continue
            except URLError, e:
                print '!!!!!!!!!!!!' + e.reason
                continue
            f.close()

            start = time.time()
            store_one(user, desc, img_file, avatar_file, addresses[i % len(addresses)])
            print 'Stored one. Consumed time: %f' % (time.time() - start)
            i += 1

    time.sleep(120)
