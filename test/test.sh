#Created by Liang Sun in 2012
#The curl in this script needs to be version 7.24 or above

curl -i -d '{"jsonrpc":"2.0","method":"register", "params":[{"email":"3@liangsun.org", "password":"1324256"}],"id":1}' http://172.16.1.206:80/api

echo "\n\n"

curl -i -d '{"jsonrpc":"2.0","method":"login", "params":[{"email":"3@liangsun.org", "password":"1324256"}],"id":1}' http://172.16.1.206:80/api -c cookie.txt

echo "\n\n"

curl -i -d '{"jsonrpc":"2.0","method":"modify_personal_information", "params":[{"show_name":"liang sun"}],"id":1}' http://172.16.1.206:80/api -b cookie.txt

echo "\n\n"

curl -i -d '{"jsonrpc":"2.0","method":"publish", "params":[{"text":"the text内容在这里。","location":{"longitude":123423,"latitude":12341234},"address":"address地址地址地址地址","type":"photo"}],"id":1}' http://172.16.1.206:80/api -b cookie.txt

echo "\n\n"

curl -i -d '{"jsonrpc":"2.0","method":"response", "params":[{"story_id":"1", "text":"You did a good job.","to":"23"}],"id":1}' http://172.16.1.206:80/api -b cookie.txt

echo "\n\n"

curl -i -F "file=@1.jpg" http://172.16.1.206:80/file -b cookie.txt
echo "\n\n"

curl -i -d '{"jsonrpc":"2.0","method":"get_outlines_by_gps","params":[{"longitude":234,"latitude":234,"offset":0}],"id":2}' http://172.16.1.206:80/api -b cookie.txt

echo "\n\n"

curl -i -d '{"jsonrpc":"2.0","method":"get_outlines_by_time","params":[{"type":"newer_than","time":0}],"id":2}' http://172.16.1.206:80/api -b cookie.txt

echo "\n\n"

curl -i -d '{"jsonrpc":"2.0","method":"get_story","params":[{"story_id":"1"}],"id":2}' http://172.16.1.206:80/api -b cookie.txt

echo "\n\n"

rm -f 240x320.jpg
curl -i http://172.16.1.206:80/file/1/1 -O -J -b cookie.txt
echo "\n\n"

rm -f 225x300.jpg
curl -i http://172.16.1.206:80/file/1/0 -O -J -b cookie.txt
echo "\n\n"

curl -i http://172.16.1.206:80/file/1/2 -b cookie.txt
echo "\n\n"

curl -i -F "file=@my_avatar.png" http://172.16.1.206:80/avatar -b cookie.txt
echo "\n\n"

rm -f avatar.png
curl -i http://172.16.1.206:80/avatar/2 -O -J -b cookie.txt
echo "\n\n"


curl -i -d '{"jsonrpc":"2.0","method":"logout", "params":[],"id":1}' http://172.16.1.206:80/api -c cookie2.txt -b cookie.txt

echo "\n\n"
