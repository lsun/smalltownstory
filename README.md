SmallTownStory README
==================

This is the server side of an Android App. 

It provides functions like storing articles and photos with geography
information, and query information by geographical location.


Getting Started
---------------

- cd <directory containing this file>

- $venv/bin/python setup.py develop

- $venv/bin/populate_SmallTownStory development.ini

- $venv/bin/pserve development.ini

