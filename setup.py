import os
import sys

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.txt')).read()
CHANGES = open(os.path.join(here, 'CHANGES.txt')).read()

requires = [
    'pyramid',
    'pyramid_rpc',
    'SQLAlchemy',
    'transaction',
    'pyramid_tm',
    'pyramid_debugtoolbar',
    'zope.sqlalchemy',
    'docutils',
#    'PIL',
    'mysql-python',
    'Paste',#needed by uWSGI
    'pastescript',#needed by uWSGI
    ]

setup(name='SmallTownStory',
      version='1.0',
      description='SmallTownStory',
      long_description=README + '\n\n' +  CHANGES,
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pylons",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='Liang Sun',
      author_email='i@liangsun.org',
      url='http://liangsun.org',
      keywords='web wsgi bfg pylons pyramid',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite='smalltownstory',
      install_requires = requires,
      entry_points = """\
      [paste.app_factory]
      main = smalltownstory:main
      [console_scripts]
      populate_SmallTownStory = smalltownstory.scripts.populate:main
      """,
      )

