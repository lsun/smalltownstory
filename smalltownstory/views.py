# encoding:utf-8
# Created by Liang Sun <i@liangsun.org> in 2012

import re, time, os, json

from pyramid.response import Response
from pyramid.httpexceptions import exception_response
from pyramid.view import view_config

from .models import (
    DBSession,
    User,
    Story,
    Comment,
    Notification,
    )

import Image

import logging
log = logging.getLogger(__name__)

from .aes import encrypt, decrypt

def error(msg, json_rpc=True):
    error_codes= {"SUCCESS" : 0,
                  "NOT_LOGGED_IN" : 1,
                  "UID_NOT_EXISTS" : 2,
                  "SID_NOT_EXISTS" : 3,
                  "INVALID_USERNAME" : 4,
                  "INVALID_PASSWORD" : 5,
                  "NO_PRIVILEGE" : 6,
                  "FILENAME_ERROR" : 7,
                  "FILE_NOT_EXISTS" : 8,
                  "IO_ERROR" : 9,
                  "PARAMETERS_ERROR" : 10,
                  "DUPLICATED_USERNAME" : 11,
                  "RESPONSE_NOT_EXISTS" : 12,
                  "UNKNOWN_ERROR" : 100,
                 }

    ans = dict(error_code=error_codes[msg],
               discript=msg)
    log.info(ans)
    if not json_rpc:
        ans = dict(jsonrpc="2.0",
                   result=ans,
                  )
        #ans = Response(json.dumps(ans))
        ans = Response(body=json.dumps(ans), status='400 Bad Request', content_type='application/json; charset=UTF-8')
        #raise exception_response(400)

    return ans

def imagesize(path):
    try:
        im = Image.open(path)
    except IOError:
        return log.error("IO Error")

    return im.size

def make_thumbnail(path, size=(300, 300)):
    base = path[0 : path.rfind('/', 0, path.rfind('/'))]
    try:
        im = Image.open(path)
    except IOError:
        return log.error("IO Error")

    mode = im.mode
    if mode not in ('L', 'RGB'):
        if mode == 'RGBA':
            #Add white background to transparent picture
            alpha = im.split()[3]
            bgmask = alpha.point(lambda x : 255 - x)
            im = im.convert('RGB')
            # paste(color, box, mask)
            im.paste((255,255,255), None, bgmask)
        else:
            im = im.convert('RGB')

    width, height = im.size
    im.thumbnail(size, Image.ANTIALIAS)
    log.debug(im.size)

    newpath = base + "/0/"
    if not os.path.exists(newpath):
        os.makedirs(newpath)

    filename = newpath + "%dx%d.jpg" % im.size
    im.save(filename, quality=100)

from pyramid_rpc.jsonrpc import jsonrpc_method
@jsonrpc_method(method='say_hello', endpoint='api')
def say_hello_view(request, lastname, firstname):
    return 'Hello, ' + lastname + " " + firstname

@jsonrpc_method(method='register', endpoint='api')
def register_view(request, params):
    log.info("Registering...")
    if not ('email' in params and 'password' in params):
        return error('PARAMETERS_ERROR')

    username = params["email"]
    password = params["password"]
    if not re.match(r'[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}', username):
        log.error(username)
        return error('INVALID_USERNAME')
    
    display_name = username[0:username.find('@')]
    log.debug('username=%s,display_name=%s,password=%s' % (username, display_name, password));
    dbsession = DBSession()

    if len(username) > 255 or len(username) < 1:
        log.error(username)
        return error('INVALID_USERNAME')

    if len(password) > 255 or len(password) < 1:
        log.error(password)
        return error('INVALID_PASSWORD')

    user = dbsession.query(User).filter(User.username == username).first()
    if user is not None:
        return error('DUPLICATED_USERNAME')

    user = User(username, display_name, password)
    dbsession.add(user)
    dbsession.flush()
    return dict(error_code = 0, user="%d" % user.id)

@jsonrpc_method(method='modify_personal_information', endpoint='api')
def modify_view(request, params):
    log.info("Modifying personal information...")
    
    if not 'show_name' in params:
        return error('PARAMETERS_ERROR')

    display_name = params["show_name"]
    log.debug(display_name)
    dbsession = DBSession()

    session = request.session
    if 'uid' not in session or 'csrf' not in session:
        return error("NOT_LOGGED_IN")

    uid = decrypt(session['uid'], session['csrf'][:16])
    if not uid:
        return error('NOT_LOGGED_IN')
    
    uid = int(uid)

    user = dbsession.query(User).filter(User.id == uid).first()
    if user is None:
        return error("UID_NOT_EXISTS")

    user.display_name = display_name
    dbsession.add(user)
    return dict(error_code = 0)

@jsonrpc_method(method='modify_password', endpoint='api')
def password_view(request, params):
    log.info("Modifying password...")
    
    if not ('old_password' in params and 'new_password' in params):
        return error('PARAMETERS_ERROR')

    old_password = params['old_password']
    new_password = params['new_password']
    log.debug(old_password)
    log.debug(new_password)
    dbsession = DBSession()

    session = request.session
    if 'uid' not in session or 'csrf' not in session:
        return error("NOT_LOGGED_IN")

    uid = decrypt(session['uid'], session['csrf'][:16])
    if not uid:
        return error('NOT_LOGGED_IN')
    
    uid = int(uid)

    user = dbsession.query(User).filter(User.id == uid).first()
    if user is None:
        return error("UID_NOT_EXISTS")

    if user.password != old_password:
        return error('INVALID_PASSWORD')

    user.password = new_password
    dbsession.add(user)
    return dict(error_code = 0)

@jsonrpc_method(method='login', endpoint='api')
def login_view(request, params):
    log.info("Logging in...")

    if not ('email' in params and 'password' in params):
        return error('PARAMETERS_ERROR')

    username = params["email"]
    password = params["password"]
    dbsession = DBSession()
    user = dbsession.query(User).filter(User.username == username).first()
    if user is None:
        return error('INVALID_USERNAME')

    if user.password != password:
        return error('INVALID_PASSWORD')

    session = request.session
    session['csrf'] = request.session.new_csrf_token()
    session['uid'] = encrypt('%d' % user.id, session['csrf'][:16])
    log.debug('csrf = ' + session['csrf'])

    return dict(error_code = 0, show_name=user.display_name, user="%d" % user.id)

@jsonrpc_method(method='logout', endpoint='api')
def logout_view(request):
    log.info("Logging out...")

    session = request.session
    if not ('uid' in session and 'csrf' in session):
        return error('NOT_LOGGED_IN')

    uid = decrypt(session['uid'], session['csrf'][:16])
    if not uid:
        return error('NOT_LOGGED_IN')
    
    uid = int(uid)

    log.debug('Poping uid:%s' % uid)
    session.pop('uid')
    session.pop('csrf')

    return dict(error_code = 0)

@jsonrpc_method(method='publish', endpoint='api')
def publish_view(request, params):
    log.info("Publishing...")
    session = request.session
    log.debug(session)
    if 'uid' not in session or 'csrf' not in session:
        return error('NOT_LOGGED_IN')

    uid = decrypt(session['uid'], session['csrf'][:16])
    if not uid:
        return error('NOT_LOGGED_IN')
    
    uid = int(uid)

    if not ('location' in params and 'longitude' in params['location'] and 'latitude' in params['location']):
        return error('PARAMETERS_ERROR')

    if not 'type' in params:
        return error('PARAMETERS_ERROR')

    type = params['type']

    longitude = params['location']['longitude']
    latitude = params['location']['latitude']
    if 'address' in params:
        address = params['address']
    else:
        address = ''

    if 'text' in params:
        text = params['text']
    else:
        text = ''

    outline = text[0:50]

    date = int(time.time())

    matrix = dict(photo=(False, True, True),
                  mark=(True, False, False),
                  thought=(True, True, False))

    if type in matrix.keys():
        valid, public, has_photo = matrix[type]
    else:
        return error('PARAMETERS_ERROR')

    if len(text) > 0:
        has_text = True
    else:
        has_text = False

    has_audio = False
    has_video = False

    story = Story(uid, longitude, latitude, address, outline, text, date, public, valid, has_photo, has_text, has_audio, has_video)
    dbsession = DBSession()
    dbsession.add(story)
    dbsession.flush()

    return dict(error_code = 0, story_id = '%d' % story.id, time = story.date)

@jsonrpc_method(method='edit', endpoint='api')
def edit_view(request, params):
    log.info("Editing...")
    session = request.session
    log.debug(session)
    if 'uid' not in session or 'csrf' not in session:
        return error('NOT_LOGGED_IN')

    uid = decrypt(session['uid'], session['csrf'][:16])
    if not uid:
        return error('NOT_LOGGED_IN')
    
    uid = int(uid)

    if not ('text' in params and 'story_id' in params):
        return error('PARAMETERS_ERROR')

    sid = params['story_id']
    text = params['text']
    outline = text[0:50]

    if len(text) > 0:
        has_text = True
    else:
        has_text = False

    dbsession = DBSession()

    story = dbsession.query(Story).filter(Story.id == sid).first()
    if story is None:
        return error('SID_NOT_EXISTS')

    if story.uid != uid:
        return error('NO_PRIVILEGE')

    story.text = text
    story.outline = outline
    story.has_text = has_text
    dbsession.add(story)

    return dict(error_code = 0)

@jsonrpc_method(method='delete_story', endpoint='api')
def delete_story_view(request, params):
    log.info("Deleting story...")
    session = request.session
    log.debug(session)
    if 'uid' not in session or 'csrf' not in session:
        return error('NOT_LOGGED_IN')

    uid = decrypt(session['uid'], session['csrf'][:16])
    if not uid:
        return error('NOT_LOGGED_IN')
    
    uid = int(uid)

    if not 'story_id' in params:
        return error('PARAMETERS_ERROR')

    sid = params['story_id']

    dbsession = DBSession()

    story = dbsession.query(Story).filter(Story.id == sid).first()
    if story is None:
        return error('SID_NOT_EXISTS')

    if story.uid != uid:
        return error('NO_PRIVILEGE')

    dbsession.delete(story)

    return dict(error_code = 0)

@jsonrpc_method(method='delete_response', endpoint='api')
def delete_response_view(request, params):
    log.info("Deleting response...")
    session = request.session
    log.debug(session)
    if 'uid' not in session or 'csrf' not in session:
        return error('NOT_LOGGED_IN')

    uid = decrypt(session['uid'], session['csrf'][:16])
    if not uid:
        return error('NOT_LOGGED_IN')
    
    uid = int(uid)

    if not 'response_id' in params:
        log.debug(params)
        return error('PARAMETERS_ERROR')

    cid = params['response_id']

    dbsession = DBSession()

    comment = dbsession.query(Comment).filter_by(id=cid).first()
    if comment is None:
        return error('RESPONSE_NOT_EXISTS')

    if comment.uid != uid:
        return error('NO_PRIVILEGE')

    dbsession.delete(comment)

    return dict(error_code = 0)

@jsonrpc_method(method='response', endpoint='api')
def response_view(request, params):
    log.info("Responsing...")
    session = request.session
    log.debug(session)
    if 'uid' not in session or 'csrf' not in session:
        return error('NOT_LOGGED_IN')

    uid = decrypt(session['uid'], session['csrf'][:16])
    if not uid:
        return error('NOT_LOGGED_IN')
    
    uid = int(uid)

    if not ('story_id' in params and 'text' in params):
        return error('PARAMETERS_ERROR')

    sid = params['story_id']
    text = params['text']
    if 'to' in params:
        to = params['to']
        log.debug(to)
    else:
        to = 0

    date = int(time.time())
    dbsession = DBSession()

    comment = Comment(sid, uid, text, date, to)
    dbsession.add(comment)
    dbsession.flush()

    cid = comment.id

    story = dbsession.query(Story).filter_by(id=sid).first()
    if story is None:
        return error('SID_NOT_EXISTS')

    origin = ''
    if story.text:
        origin = story.text
    elif story.has_photo:
        origin = '图片'

    if story.uid != uid:
        notif1 = dbsession.query(Notification).filter_by(to=story.uid).filter_by(sid=sid).first()
        if notif1 is None:
            notif1 = Notification(story.uid, sid, origin, True)
            dbsession.add(notif1)
        else:
            notif1.count += 1
            dbsession.add(notif1)

    if to and to != story.uid and to != uid:
        notif2 = dbsession.query(Notification).filter_by(to=to).filter_by(sid=sid).first()
        if notif2 is None:
            notif2 = Notification(to, sid, origin, False)
            dbsession.add(notif2)
        else:
            notif2.count += 1
            dbsession.add(notif2)

    return dict(error_code = 0, time = date, response_id = cid)

@jsonrpc_method(method='get_outlines_by_gps', endpoint='api')
def get_outlines_by_gps_view(request, params):
    log.info("Getting outlines by gps...")
    session = request.session

    if not ('longitude' in params and 'latitude' in params and 'offset' in params):
        return error('PARAMETERS_ERROR')

    longitude = params['longitude']
    latitude = params['latitude']
    offset = params['offset']

    dbsession = DBSession()
    items = dbsession.query("id","uid","longitude","latitude","outline","date","address",
                            "photo_width","photo_height","has_photo","public","display_name"
                           ).from_statement("select s.id,s.uid,s.longitude,s.latitude,s.outline,"
                                            "s.date,s.address,s.photo_width,s.photo_height,s.has_photo,"
                                            "s.public,users.display_name "
                                            "from (select * from stories where public=1 and valid=1 "
                                            "order by abs(longitude-:longitude)+abs(latitude-:latitude) "
                                            "limit :offset,20) as s inner join users on users.id = s.uid"
                                           ).params(longitude=longitude,latitude=latitude,offset=offset).all()

    ans = {}
    ans['error_code'] = 0
    ans['stories_breviary'] = []
    for item in items:
        one = {}
        one['story_id'] = '%d' % item.id
        one['user'] = '%d' % item.uid
        one['show_name'] = item.display_name
        one['location'] = {'longitude':item.longitude,'latitude':item.latitude}
        one['address'] = item.address
        one['time'] = item.date
        if item.has_photo:
            one['type'] = 'photo'
            one['width'] = item.photo_width
            one['height'] = item.photo_height
        else:
            one['type'] = 'thought'

        one['abstract'] = item.outline
        one['like_count'] = 0
        one['responce_count'] = 0
        ans['stories_breviary'].append(one)

    return ans

@jsonrpc_method(method='get_outlines_by_time', endpoint='api')
def get_outlines_by_time_view(request, params):
    log.info("Getting outlines by time")
    session = request.session
    
    if not ('uid' in session and 'csrf' in session):
        return error('NOT_LOGGED_IN')

    if not ('type' in params and 'time' in params):
        return error('PARAMETERS_ERROR')

    uid = decrypt(session['uid'], session['csrf'][:16])
    if not uid:
        return error('NOT_LOGGED_IN')
    
    uid = int(uid)

    timetype = params['type']
    timeline = params['time']

    dbsession = DBSession()
    query_string = ''
    if 'newer_than' == timetype:
        cmp = '>'
    else:
        cmp = '<'

    query_string = ("select s.id,s.uid,s.longitude,s.latitude,s.outline,s.date,s.address,"
    "s.photo_width,s.photo_height,s.has_photo,s.public,users.display_name"
    " from (select * from stories where date " + cmp + " :timeline and "
    "uid = :uid order by date desc limit 20) as s inner join users on users.id = s.uid")

    items = dbsession.query("id","uid","longitude","latitude","outline","date","address",
                            "photo_width","photo_height","has_photo","public",
                            "display_name").from_statement(query_string).params(timeline=timeline,uid=uid).all()

    ans = {}
    ans['error_code'] = 0
    ans['stories_breviary'] = []
    for item in items:
        one = {}
        one['story_id'] = '%d' % item.id
        one['user'] = '%d' % item.uid
        one['show_name'] = item.display_name
        one['location'] = {'longitude':item.longitude,'latitude':item.latitude}
        one['address'] = item.address
        one['time'] = item.date

        if item.has_photo:
            one['type'] = 'photo'
            one['width'] = item.photo_width
            one['height'] = item.photo_height
        elif item.public:
            one['type'] = 'thought'
        else:
            one['type'] = 'mark'

        one['abstract'] = item.outline
        one['like_count'] = 0
        one['responce_count'] = 0
        ans['stories_breviary'].append(one)

    return ans

@jsonrpc_method(method='query_unread_msg', endpoint='api')
def get_notifications_view(request):
    log.info("Getting notifications...")
    session = request.session
    
    if not ('uid' in session and 'csrf' in session):
        return error('NOT_LOGGED_IN')

    uid = decrypt(session['uid'], session['csrf'][:16])
    if not uid:
        return error('NOT_LOGGED_IN')
    
    uid = int(uid)

    dbsession = DBSession()
    notifications = dbsession.query(Notification).filter_by(to=uid).limit(20).all()

    ans = {}
    ans['error_code'] = 0
    ans['msgs'] = []

    for notification in notifications:
        msg = {}
        msg['story_id'] = notification.sid
        msg['origin'] = notification.origin
        msg['count'] = notification.count
        msg['type'] = int(notification.is_author)
        ans['msgs'].append(msg)

    return ans

@jsonrpc_method(method='mark_msg_as_read', endpoint='api')
def delete_notifications_view(request, params):
    log.info("Deleting notifications...")
    session = request.session
    
    if not ('uid' in session and 'csrf' in session):
        return error('NOT_LOGGED_IN')

    if not ('story_ids' in params):
        return error('PARAMETERS_ERROR')

    uid = decrypt(session['uid'], session['csrf'][:16])
    if not uid:
        return error('NOT_LOGGED_IN')
    
    uid = int(uid)

    sids = params['story_ids']

    dbsession = DBSession()
    dbsession.query(Notification).filter_by(to=uid).filter(Notification.sid.in_(sids)).delete('fetch')

    ans = dict(error_code=0)
    return ans

@jsonrpc_method(method='get_story', endpoint='api')
def get_story_view(request, params):
    log.info("Getting story...")
    session = request.session
    if 'story_id' not in params:
        return error('PARAMETERS_ERROR')

    sid = params['story_id']

    dbsession = DBSession()
    item = dbsession.query(Story).filter_by(id=sid).first()
    if item is None:
        return error('SID_NOT_EXISTS')

    user = dbsession.query(User).filter_by(id=item.uid).first()
    if item is None:
        return error('UID_NOT_EXISTS')

    comments = dbsession.query(Comment).filter_by(sid=sid).order_by(Comment.date).all()

    ans = {}
    ans['error_code'] = 0
    ans['story_id'] = sid
    ans['text'] = item.text
    ans['user'] = '%d' % item.uid
    ans['show_name'] = user.display_name
    ans['location'] = {'longitude':item.longitude,'latitude':item.latitude}
    ans['address'] = item.address
    ans['time'] = item.date
    
    if item.has_photo:
        ans['type'] = 'photo'
        ans['width'] = item.photo_width
        ans['height'] = item.photo_height
    elif item.public:
        ans['type'] = 'thought'
    else:
        ans['type'] = 'mark'

    ans['like_count'] = 0
    ans['responce_count'] = 0
    ans['i_like'] = False

    
    ans['response'] = []
    if os.path.exists('smalltownstory/images/%s/1/' % sid):
        ans['type'] = 'photo'
    elif len(ans['text']) == 0:
        ans['type'] = 'mark'
    else:
        ans['type'] = 'thought'

    for comment in comments:
        o_comment = {}
        o_comment["response_id"] = '%d' % comment.id
        o_comment["user"] = '%d' % comment.uid
        user = dbsession.query(User).filter(User.id == comment.uid).first()
        if user is not None:
            o_comment['show_name'] = user.display_name

        user2 = dbsession.query(User).filter(User.id == comment.to).first()
        if user2 is not None:
            o_comment["to"] = dict(user=comment.to, show_name=user2.display_name)

        o_comment["text"] = comment.text
        o_comment["time"] = comment.date
        ans['response'].append(o_comment)

    return ans

@view_config(route_name='upload_file', renderer='json')
def upload_file_view(request):
    log.info("Uploading file...")
    session = request.session
    if 'uid' not in session or 'csrf' not in session:
        return error('NOT_LOGGED_IN', False)

    uid = decrypt(session['uid'], session['csrf'][:16])
    if not uid:
        return error('NOT_LOGGED_IN', False)
    
    uid = int(uid)

    if len(request.params.items()) < 1 or len(request.params.items()[0]) < 2:
        return error('FILE_NOT_EXISTS', False)

    for item in request.params.items():
        fileitem = item[1]
        filename = fileitem.filename
        log.debug(filename)
        m = re.match(r'(\d+)\.(\w{3,4})', filename)
        if m is None:
            return error('FILENAME_ERROR', False)

        log.debug(m.groups())

        sid = m.group(1)
        suffix = m.group(2)

        dbsession = DBSession()
        story = dbsession.query(Story).filter(Story.id == sid).first()
        if story is None:
            return error('SID_NOT_EXISTS', False)

        if story.uid != uid:
            return error('NO_PRIVILEGE', False)

        destination = "images/%s/1/a" % sid

        log.debug(destination)
        filepath = 'smalltownstory/' + destination

        dir = os.path.dirname(filepath)
        log.debug(os.path.exists(dir))
        if not os.path.exists(dir):
            os.makedirs(dir)
        else:
            dirlist = os.listdir(dir)
            for filename in dirlist:
                log.debug(dir + "/" + filename)
                os.remove(dir + "/" + filename)

        open(filepath, 'wb').write(fileitem.file.read())
        make_thumbnail(filepath)
        size = imagesize(filepath)
        os.rename(dir + "/a", dir + "/%dx%d." % size + suffix)

        story.has_photo = True
        story.valid = True
        story.photo_width = size[0]
        story.photo_height = size[1]
        dbsession.add(story)

    ans = {'jsonrpc' : '2.0',
           'result' : {'error_code' : 0}
          }
    return ans

@view_config(route_name='upload_avatar', renderer='json')
def upload_avatar_view(request):
    log.info("Uploading avatar...")
    log.debug(request.params)
    log.debug(request.session)

    session = request.session
    if 'uid' not in session or 'csrf' not in session:
        return error('NOT_LOGGED_IN', False)

    uid = decrypt(session['uid'], session['csrf'][:16])
    if not uid:
        return error('NOT_LOGGED_IN', False)
    
    uid = int(uid)

    if len(request.params.items()) < 1 or len(request.params.items()[0]) < 2:
        return error('FILE_NOT_EXISTS', False)

    fileitem = request.params.items()[0][1]
    filename = fileitem.filename
    log.debug(filename)
    m = re.match(r'(.+)\.(\w{3,4})', filename)
    if m is None:
        return error('FILENAME_ERROR', False)

    log.debug(m.groups())

    suffix = m.group(2)
    destination = "avatars/%d/avatar.%s" % (uid, suffix)

    log.debug(destination)
    filepath = 'smalltownstory/' + destination

    dir = os.path.dirname(filepath)
    log.debug(os.path.exists(dir))
    if not os.path.exists(dir):
        os.makedirs(dir)
    else:
        dirlist = os.listdir(dir)
        for filename in dirlist:
            log.debug("Removing direction: " + dir + "/" + filename)
            os.remove(dir + "/" + filename)

    open(filepath, 'wb').write(fileitem.file.read())

    ans = {'jsonrpc' : '2.0',
           'result' : {'error_code' : 0}
          }
    return ans

@view_config(route_name='download_file')
def download_file(request):
    log.info("Downloading file...")
    log.debug(request.params)
    story_id = request.matchdict['story_id']
    sub_id = request.matchdict['sub_id']

    path = 'smalltownstory/images/%s/%s/' % (story_id, sub_id)
    if not os.path.exists(path):
        return error('FILE_NOT_EXISTS', False)

    dirlist = os.listdir(path)
    if dirlist is None:
        return error('FILE_NOT_EXISTS', False)
    
    if len(dirlist) != 1:
        return error('IO_ERROR', False)

    filename = dirlist[0]
    log.debug(filename)
    size = os.path.getsize(path + filename)
    
    response = Response(content_type='application/force-download', content_disposition='attachment; filename=' + filename)
    response.app_iter = open(path + filename, 'rb')
    response.content_length = size
    return response

@view_config(route_name='download_avatar')
def download_avatar(request):
    log.info("Downloading avatar...")
    log.debug(request.params)
    user_id = request.matchdict['user_id']

    path = 'smalltownstory/avatars/%s/' % user_id
    log.debug(path)
    if not os.path.exists(path):
        return error('FILE_NOT_EXISTS', False)

    dirlist = os.listdir(path)
    if len(dirlist) != 1:
        return error('IO_ERROR', False)

    filename = dirlist[0]
    log.debug(filename)
    size = os.path.getsize(path + filename)
    response = Response(content_type='application/force-download', content_disposition='attachment; filename=' + filename)
    response.app_iter = open(path + filename, 'rb')
    response.content_length = size
    return response
