from pyramid.config import Configurator
from pyramid.session import UnencryptedCookieSessionFactoryConfig

# from pyramid.authentication import AuthTktAuthenticationPolicy
# from pyramid.authorization import ACLAuthorizationPolicy
# from smalltownstory.security import groupfinder

from sqlalchemy import engine_from_config


from .models import DBSession

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    engine = engine_from_config(settings, 'sqlalchemy.', pool_recycle=3600)
    #engine = engine_from_config(settings, 'sqlalchemy.')
    
    DBSession.configure(bind=engine)
    my_session_factory = UnencryptedCookieSessionFactoryConfig('itsaseekreet')

    config = Configurator(settings=settings, session_factory=my_session_factory)
    
    # authn_policy = AuthTktAuthenticationPolicy(
    #     'sosecret', callback=groupfinder)
    # authz_policy = ACLAuthorizationPolicy()
    # config = Configurator(settings=settings,
    #                       root_factory='smalltownstory.models.RootFactory',
    #                       authentication_policy=authn_policy,
    #                       authorization_policy=authz_policy)
    
#    config.add_static_view('static', 'static', cache_max_age=3600)
#    config.add_static_view('images', 'smalltownstory:images/')
#    config.add_static_view('avatars', 'smalltownstory:avatars/')
    config.include('pyramid_rpc.jsonrpc')
    config.add_jsonrpc_endpoint('api', '/api')
    config.add_route('upload_file', '/file')
    config.add_route('upload_avatar', '/avatar')
    config.add_route('download_file', '/file/{story_id}/{sub_id}')
    config.add_route('download_avatar', '/avatar/{user_id}')
    config.scan()

    app = config.make_wsgi_app()
    #from paste.translogger import TransLogger
    #app = TransLogger(app, setup_console_handler=False)
    
    return app
