#encoding:utf-8
#Created by Liang Sun <i@liangsun.org> in 2012

import random
import json
from shutil import copyfile
import hashlib
import collections
import time
import urllib2
import MultipartPostHandler
from urllib2 import URLError, HTTPError

REQUESTS = [
    ('http://172.16.1.206:80/api', '{"jsonrpc":"2.0","method":"register", "params":[{"email":"3@liangsun.org", "password":"1324256"}],"id":1}'),
    ('http://172.16.1.206:80/api', '{"jsonrpc":"2.0","method":"login", "params":[{"email":"3@liangsun.org", "password":"1324256"}],"id":1}'),
    ('http://172.16.1.206:80/api', '{"jsonrpc":"2.0","method":"modify_personal_information", "params":[{"show_name":"liang sun"}],"id":1}'),
    ('http://172.16.1.206:80/api', '{"jsonrpc":"2.0","method":"publish", "params":[{"text":"the text","location":{"longitude":123423,"latitude":12341234}}],"id":1}'),
    ('http://172.16.1.206:80/api', '{"jsonrpc":"2.0","method":"response", "params":[{"story_id":"1", "text":"You did a good job.","to":["23"]}],"id":1}'),
    ('http://172.16.1.206:80/file', {'file':open('1.jpg', 'rb')}),
    ('http://172.16.1.206:80/api', '{"jsonrpc":"2.0","method":"get_outlines_by_gps","params":[{"longitude":234,"latitude":234,"offset":0}],"id":2}'),
    ('http://172.16.1.206:80/api', '{"jsonrpc":"2.0","method":"get_outlines_by_time","params":[{"type":"newer_than","time":0}],"id":2}'),
    ('http://172.16.1.206:80/api', '{"jsonrpc":"2.0","method":"get_story","params":[{"story_id":"1"}],"id":2}'),
    ('http://172.16.1.206:80/file/1/0', None),
    ('http://172.16.1.206:80/file/1/1', None),
    ('http://172.16.1.206:80/file/1/2', None),
    ('http://172.16.1.206:80/avatar', {'file':open('my_avatar.png', 'rb')}),
    ('http://172.16.1.206:80/avatar/2', None),
]

request_stats = collections.defaultdict(list)

def record_request(url, start, finish):
    """ Store request start/stop time into a global stats dict. """
    global request_stats
    request_stats[url].append((start, finish))

def perform_request(opener, url, data):
    start = time.time()
    ans = False
    try:
        ans = opener.open(url, data=data).read()
        #print ans
    except HTTPError, e:
        print 'Error code: ', e.code
    except URLError, e:
        print e.reason
    record_request(url, start, time.time())
    print 'Consumed time:%f' % (time.time() - start)
    return ans

def unit_test():
    opener = urllib2.build_opener(
        urllib2.HTTPCookieProcessor(), urllib2.HTTPRedirectHandler(), MultipartPostHandler.MultipartPostHandler)

    opener2 = urllib2.build_opener(
        urllib2.HTTPCookieProcessor(), urllib2.HTTPRedirectHandler(), MultipartPostHandler.MultipartPostHandler)

    username = "test_" + "%d" % int(random.uniform(100000000, 1000000000))
    username2 = "test_" + "%d" % int(random.uniform(100000000, 1000000000))

    email = username + "@liang.sun"
    email2 = username2 + "@liang.sun"

    password = hashlib.sha256(username).hexdigest()
    password2 = hashlib.sha256(username2).hexdigest()

    url = 'http://172.16.1.206:80/api'

    print "Registering..."
    data = '{"jsonrpc":"2.0","method":"register", "params":[{"email":"' + email + '", "password":"' + password + '"}],"id":1}'
    ret = perform_request(opener, url, data)
    if not ret:
        return

    data2 = '{"jsonrpc":"2.0","method":"register", "params":[{"email":"' + email2 + '", "password":"' + password2 + '"}],"id":1}'
    ret = perform_request(opener2, url, data2)
    if not ret:
        return


    print "Logging in..."
    data = '{"jsonrpc":"2.0","method":"login", "params":[{"email":"' + email + '", "password":"' + password + '"}],"id":1}'
    ret = perform_request(opener, url, data)
    if not ret:
        return

    data2 = '{"jsonrpc":"2.0","method":"login", "params":[{"email":"' + email2 + '", "password":"' + password2 + '"}],"id":1}'
    ret = perform_request(opener2, url, data2)
    if not ret:
        return

    print "Modifying show_name..."
    show_name = '测试'
    data = '{"jsonrpc":"2.0","method":"modify_personal_information", "params":[{"show_name":"' + show_name + '"}],"id":1}'
    ret = perform_request(opener, url, data)
    if not ret:
        return

    print 'Publishing...'
    text = '测试内容'
    longitude = 1
    latitude = 1
    address = '测试地址'

    data = '{"jsonrpc":"2.0","method":"publish", "params":[{"text":"' + text
    data = data + '","location":{"longitude":%d,"latitude":%d},"address":"' % (longitude, latitude)
    data = data + address
    data = data + '","type":"photo"}],"id":1}'
    ret = perform_request(opener, url, data)
    if not ret:
        return

    print ret

    result = json.loads(ret)['result']
    if result['error_code'] != 0:
        return
    sid = result['story_id'].encode('utf-8') #For response
    print 'sid = ' + sid

    print 'Responding...'
    text = '回复回复'
    data2 = '{"jsonrpc":"2.0","method":"response", "params":[{"story_id":"' + sid + '", "text":"' + text + '"}],"id":1}'
    ret = perform_request(opener2, url, data2)
    if not ret:
        return

    print ret

    print 'Uploading image...'
    url = 'http://172.16.1.206:80/file'
    filename = sid + '.jpg'
    copyfile('2.jpg', filename)
    data = {'file':open(filename, 'rb')}
    ret = perform_request(opener, url, data)
    if not ret:
        return

    print 'Uploading avatar...'
    url = 'http://172.16.1.206:80/avatar'
    data = {'file':open('my_avatar.png', 'rb')}
    ret = perform_request(opener, url, data)
    if not ret:
        return
    
    url = 'http://172.16.1.206:80/api'
    print 'Getting outlines by gps...'
    data = '{"jsonrpc":"2.0","method":"get_outlines_by_gps","params":[{"longitude":234,"latitude":234,"offset":0}],"id":2}'
    ret = perform_request(opener, url, data)
    if not ret:
        return
    
    print 'Getting outlines by time...'
    data = '{"jsonrpc":"2.0","method":"get_outlines_by_time","params":[{"type":"newer_than","time":0}],"id":2}'
    ret = perform_request(opener, url, data)
    if not ret:
        return
    
    print 'Getting story...'
    data = '{"jsonrpc":"2.0","method":"get_story","params":[{"story_id":"' + sid + '"}],"id":2}'
    ret = perform_request(opener, url, data)
    if not ret:
        return
    
    print 'Getting images...'
    url = 'http://172.16.1.206:80/file/' + sid + '/1'
    data = ''
    ret = perform_request(opener, url, data)
    if not ret:
        return

    print 'Getting images thumbnail...'
    url = 'http://172.16.1.206:80/file/' + sid + '/0'
    data = ''
    ret = perform_request(opener, url, data)
    if not ret:
        return

    print 'Query unread msg...'
    url = 'http://172.16.1.206:80/api'
    data = '{"jsonrpc":"2.0","method":"query_unread_msg","params":[],"id":2}'
    ret = perform_request(opener, url, data)
    if not ret:
        return
    print ret

    print 'Deleting stroy...'
    url = 'http://172.16.1.206:80/api'
    data = '{"jsonrpc":"2.0","method":"delete_story","params":[{"story_id":"' + sid + '"}],"id":2}'
    ret = perform_request(opener, url, data)
    if not ret:
        return
    print ret


    print 'Getting avatar...'
    url = 'http://172.16.1.206:80/avatar/1'
    data = ''
    ret = perform_request(opener, url, data)
    if not ret:
        return

    print 'Finished unit test.'

unit_test()
