#!/usr/bin/python

"""
Load tester for visiting a website, viewing a product
page, and adding that product to one's cart.
"""

import random
import hashlib
from gevent import pool
from gevent import monkey

monkey.patch_all()

import collections
import time
import urllib2
import MultipartPostHandler
from urllib2 import URLError, HTTPError

POOL_SIZE = 500 # number of concurrent sessions
MAX_SESSIONS = 10000 # total number of sessions to test
REQUESTS = [
    # (URL, HTTP post data) tuples
    ('http://story.hada.me:8080/api', '{"jsonrpc":"2.0","method":"register", "params":[{"email":"3@liangsun.org", "password":"1324256"}],"id":1}'),
    ('http://story.hada.me:8080/api', '{"jsonrpc":"2.0","method":"login", "params":[{"email":"3@liangsun.org", "password":"1324256"}],"id":1}'),
    ('http://story.hada.me:8080/api', '{"jsonrpc":"2.0","method":"modify_personal_information", "params":[{"show_name":"liang sun"}],"id":1}'),
    ('http://story.hada.me:8080/api', '{"jsonrpc":"2.0","method":"publish", "params":[{"text":"the text","location":{"longitude":123423,"latitude":12341234}}],"id":1}'),
    ('http://story.hada.me:8080/api', '{"jsonrpc":"2.0","method":"response", "params":[{"story_id":"1", "text":"You did a good job.","to":["23"]}],"id":1}'),
    ('http://story.hada.me:8080/file', {'file':open('1.jpg', 'rb')}),
    ('http://story.hada.me:8080/api', '{"jsonrpc":"2.0","method":"get_outlines_by_gps","params":[{"longitude":234,"latitude":234,"offset":0}],"id":2}'),
    ('http://story.hada.me:8080/api', '{"jsonrpc":"2.0","method":"get_outlines_by_time","params":[{"type":"newer_than","time":0}],"id":2}'),
    ('http://story.hada.me:8080/api', '{"jsonrpc":"2.0","method":"get_story","params":[{"story_id":"1"}],"id":2}'),
    ('http://story.hada.me:8080/file/1/0', None),
    ('http://story.hada.me:8080/file/1/1', None),
    ('http://story.hada.me:8080/file/1/2', None),
    ('http://story.hada.me:8080/avatar', {'file':open('my_avatar.png', 'rb')}),
    ('http://story.hada.me:8080/avatar/2', None),
]

request_stats = collections.defaultdict(list)

def record_request(url, start, finish):
    """ Store request start/stop time into a global stats dict. """
    global request_stats
    request_stats[url].append((start, finish))

def perform_request(opener, url, data):
    start = time.time()
    ans = False
    try:
        ans = opener.open(url, data=data).read()
    except HTTPError, e:
        print 'Error code: ', e.code
    except URLError, e:
        print e.reason
    record_request(url, start, time.time())
    return ans

def cart_session():
    """
    Simulates a user landing on the home page, clicking on a product,
    and then adding it to cart.
    """
    opener = urllib2.build_opener(
        urllib2.HTTPCookieProcessor(), urllib2.HTTPRedirectHandler(), MultipartPostHandler.MultipartPostHandler)
    
    username = "test_" + "%d" % int(random.uniform(100000000, 1000000000))
    email = username + "@liang.sun"
    password = hashlib.sha256(username).hexdigest()
    url = 'http://story.hada.me:8080/api'
    data = '{"jsonrpc":"2.0","method":"register", "params":[{"email":"' + email + '", "password":"' + password + '"}],"id":1}'
    
    print "Registering..."
    ret = perform_request(opener, url, data)
    if not ret:
        return

    
    data = '{"jsonrpc":"2.0","method":"login", "params":[{"email":"' + email + '", "password":"' + password + '"}],"id":1}'
    
    print "Logging in..."
    perform_request(opener, url, data)
    
request_pool = pool.Pool(POOL_SIZE)
queued_sessions = 0 
while queued_sessions < MAX_SESSIONS:
    request_pool.spawn(cart_session)
    queued_sessions += 1
    print 'Queued: %d' % queued_sessions

request_pool.join()
